<div class="content-wrapper">

  <section class="content-header">

    <h1>

      Products management

    </h1>

    <ol class="breadcrumb">

      <li><a href="home"><i class="fa fa-dashboard"></i> Home</a></li>

      <li class="active">Dashboard</li>

    </ol>

  </section>

  <section class="content">

    <div class="box">

      <div class="box-header with-border">

        <button class="btn btn-primary" data-toggle="modal" data-target="#addProduct">

          Add Product

        </button>

      </div>

      <div class="box-body">

        <table class="table table-bordered table-striped dt-responsive tables" width="100%">
       
          <thead>
           
           <tr>
             
             <th style="width:10px">#</th>
             <th>Image</th>
             <th>Code</th>
             <th>Description</th>
             <th>Category</th>
             <th>Stock</th>
             <th>Buying price</th>
             <th>Selling Price</th>
             <th>Date added</th>
             <th>Actions</th>

           </tr> 

          </thead>

          <tbody>

            <?

              $item = null;
              $value = null;

              $products = ControllerProducts::ctrShowProducts($item, $value);

                    foreach ($products as $key => $value) {
                      
                      echo '<tr>
                            <td>'.($key+1).'</td>
                            <td><img src="views/img/products/default/anonymous.png" class="img-thumbnail" width="40px"></td>
                            <td>'.$value['code'].'</td>
                            <td>'.$value['description'].'</td>';
                            
                            $item = 'id';
                            $value = $value['idCategory'];

                            $category = ControllerCategories::ctrShowCategories($item, $value);

                            echo '<td>'.$category['category'].'</td>
                                  <td>'.$value['stock'].'</td>
                                  <td>৳'.$value['buyingPrice'].'</td>
                                  <td>৳'.$value['sellingPrice'].'</td>
                                  <td>'.$value['date'].'</td>
                                  <td>

                                    <div class="btn-group">
                                        
                                      <button class="btn btn-warning"><i class="fa fa-pencil"></i></button>

                                      <button class="btn btn-danger"><i class="fa fa-times"></i></button>

                                    </div>  

                                  </td>

                            </tr>';

                    }

            ?>

          </tbody>

        </table>

      </div>
    
    </div>

  </section>

</div>


<!--=====================================
=            module add Product            =
======================================-->

<!-- Modal -->
<div id="addProduct" class="modal fade" role="dialog">

  <div class="modal-dialog">

    <!-- Modal content-->

    <div class="modal-content">

      <form role="form" method="POST" enctype="multipart/form-data">

        <div class="modal-header" style="background: #3c8dbc; color: #fff">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">Add Product</h4>

        </div>

        <div class="modal-body">

          <div class="box-body">

            <div class="form-group">

              <div class="input-group">

                <span class="input-group-addon"><i class="fa fa-th"></i></span>

                <select class="form-control input-lg" id="newCategory" name="newCategory">

                  <option value="">Select Category</option>
                  <option value="Baby Items">Baby Items</option>
                  <option value="Baby Items">Baby Items</option>
                  <option value="Baby Items">Baby Items</option>

                </select>

              </div>

            </div>

            <div class="form-group">

              <div class="input-group">

                <span class="input-group-addon"><i class="fa fa-barcode"></i></span>

                <input class="form-control input-lg" type="text" id="newCode" name="newCode" placeholder="Add Product Code" required>
                
              </div>

            </div>

            <div class="form-group">

              <div class="input-group">

                <span class="input-group-addon"><i class="fa fa-product-hunt"></i></span>

                <input class="form-control input-lg" type="text" id="newDescription" name="newDescription" placeholder="Add Description" required>

              </div>

            </div>

            <div class="form-group">

              <div class="input-group">

                <span class="input-group-addon"><i class="fa fa-tasks"></i></span>

                <input class="form-control input-lg" type="number" id="newStock" name="newStock" placeholder="Add Stock" min="0" required>

              </div>

            </div>

            <div class="form-group row">

              <div class="col-xs-12 col-sm-6">

                <div class="input-group"> 

                  <span class="input-group-addon"><i class="fa fa-arrow-up"></i></span> 

                  <input type="number" class="form-control input-lg" id="newBuyingPrice" name="newBuyingPrice" step="any" min="0" placeholder="Buying price" required>

                </div>

              </div>

              <div class="col-xs-12 col-sm-6">  

                <div class="input-group"> 

                  <span class="input-group-addon"><i class="fa fa-arrow-down"></i></span> 

                  <input type="number" class="form-control input-lg" id="newSellingPrice" name="newSellingPrice" step="any" min="0" placeholder="Selling price" required>

                </div> 

                <br>

                <div class="col-xs-6"> 

                  <div class="form-group">   

                    <label>     

                      <input type="checkbox" class="minimal percentage" checked>

                      Use percentage

                    </label>

                  </div>

                </div>

                <div class="col-xs-6" style="padding:0">

                  <div class="input-group"> 

                    <input type="number" class="form-control input-lg newPercentage" min="0" value="40" required>

                    <span class="input-group-addon"><i class="fa fa-percent"></i></span>

                  </div>

                </div>

              </div>

            </div>

            <div class="form-group">

              <div class="panel">Upload image</div>

              <input id="newProdPhoto" type="file" class="newImage" name="newProdPhoto">

              <p class="help-block">Maximum size 2Mb</p>

              <img src="views/img/products/default/anonymous.png" class="img-thumbnail preview" alt="" width="100px">

            </div> 

          </div>

          </div>

          <div class="modal-footer">

          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>

          <button type="submit" class="btn btn-primary">Save product</button>

        </div>

        </div>

      </form>

    </div>

  </div>

</div>

<!--====  End of module add user  ====-->
